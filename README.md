# Money transfer Rest API

RESTful API for money transfers between users accounts

### How to run
```sh
mvn package
cd api
java -jar target/money-transfer.api-1.0-SNAPSHOT.jar server config.yml
```

Application starts a jetty server on localhost port 8080

### Available Requests

| HTTP METHOD | PATH | USAGE |
| -----------| ------ | ------ |
| POST | /moneytransfer/account | create new account | 
| GET | /moneytransfer/account?id={ID}  | get account by ID| 
| GET | /moneytransfer/account/ | get all accounts | 
| DELETE | /moneytransfer/account?id={ID}  | delete account | 
| POST | /moneytransfer/transfer?from={FROM}&to={TO}&amount={AMOUNT} | transfer AMOUNT from account with id=FROM to account with id=TO | 

### Sample JSON
##### Account: : 

```sh
{  
  "id":1,
  "balance":"200.00"
} 
```

#### TransferResult:
```sh
{  
   "success": true
}
```