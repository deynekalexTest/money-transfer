package com.deyneka.moneytransfer

import com.deyneka.moneytransfer.dao.AccountDAO
import com.deyneka.moneytransfer.entity.AccountEntity
import com.deyneka.moneytransfer.resource.AccountResource
import com.deyneka.moneytransfer.resource.TransferResource
import io.dropwizard.Application
import io.dropwizard.db.DataSourceFactory
import io.dropwizard.hibernate.HibernateBundle
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment

class MoneyTransferApplication : Application<MoneyTransferConfiguration>() {

    private val hibernate = object : HibernateBundle<MoneyTransferConfiguration>(AccountEntity::class.java) {
        override fun getDataSourceFactory(configuration: MoneyTransferConfiguration): DataSourceFactory {
            return configuration.dataSourceFactory
        }
    }

    override fun getName(): String {
        return "money-transfer"
    }

    override fun initialize(bootstrap: Bootstrap<MoneyTransferConfiguration>) {
        bootstrap.addBundle(hibernate)
    }

    override fun run(moneyTransferConfiguration: MoneyTransferConfiguration, environment: Environment) {
        val accountDAO = AccountDAO(hibernate.sessionFactory)
        environment.jersey().register(AccountResource(accountDAO))
        environment.jersey().register(TransferResource(accountDAO))
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            MoneyTransferApplication().run(*args)
        }
    }
}
