package com.deyneka.moneytransfer

import com.fasterxml.jackson.annotation.JsonProperty
import io.dropwizard.Configuration
import io.dropwizard.db.DataSourceFactory
import javax.validation.Valid
import javax.validation.constraints.NotNull

class MoneyTransferConfiguration : Configuration() {
    @Valid
    @NotNull
    @JsonProperty("database")
    internal var dataSourceFactory = DataSourceFactory()
        private set

    fun setDatabase(database: DataSourceFactory) {
        this.dataSourceFactory = database
    }
}