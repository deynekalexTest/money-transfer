package com.deyneka.moneytransfer.api

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

data class Account(
        @JsonProperty
        var id: Long = 0,

        @JsonProperty
        var balance: BigDecimal = BigDecimal.ZERO
)
