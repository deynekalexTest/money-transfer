package com.deyneka.moneytransfer.api

import com.fasterxml.jackson.annotation.JsonProperty

data class TransferResult(
        @JsonProperty
        var success: Boolean = false
)