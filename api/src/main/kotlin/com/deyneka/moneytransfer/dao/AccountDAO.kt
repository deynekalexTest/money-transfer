package com.deyneka.moneytransfer.dao

import com.deyneka.moneytransfer.entity.AccountEntity
import io.dropwizard.hibernate.AbstractDAO
import org.hibernate.SessionFactory
import java.math.BigDecimal

class AccountDAO(factory: SessionFactory) : AbstractDAO<AccountEntity>(factory) {

    fun findById(id: Long): AccountEntity? {
        return get(id)
    }

    fun create(accountEntity: AccountEntity): AccountEntity {
        return persist(accountEntity)
    }

    fun delete(accountEntity: AccountEntity) {
        currentSession().delete(accountEntity)
    }

    fun findAll(): List<AccountEntity> {
        val query = this.currentSession().criteriaBuilder.createQuery(AccountEntity::class.java)
        query.from(AccountEntity::class.java)
        return list(query)
    }

    fun transfer(from: Long, to: Long, amount: BigDecimal): Boolean {
        val accountFrom = findById(from)
        val accountTo = findById(to)
        if (accountFrom != null && accountTo != null && accountFrom != accountTo && amount > BigDecimal.ZERO && accountFrom.balance >= amount) {
            accountFrom.balance -= amount
            accountTo.balance += amount
            currentSession().transaction.commit()
            return true
        }
        return false
    }
}
