package com.deyneka.moneytransfer.entity

import java.math.BigDecimal
import javax.persistence.*
import javax.validation.constraints.Digits

@Entity
@Table(name = "account")
data class AccountEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0,

    @Digits(integer = 36, fraction = 2)
    @Column
    var balance: BigDecimal = BigDecimal.ZERO
)
