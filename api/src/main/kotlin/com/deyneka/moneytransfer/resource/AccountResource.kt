package com.deyneka.moneytransfer.resource

import com.deyneka.moneytransfer.api.Account
import com.deyneka.moneytransfer.dao.AccountDAO
import com.deyneka.moneytransfer.entity.AccountEntity
import io.dropwizard.hibernate.UnitOfWork

import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import java.math.BigDecimal

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
class AccountResource(private val accountDAO: AccountDAO) {

    @POST
    @UnitOfWork
    fun createAccount(account: Account): Response {
        val createdAccountEntity = accountDAO.create(AccountEntity(balance = account.balance))
        return Response.ok()
                .entity(Account(createdAccountEntity.id, createdAccountEntity.balance)).build()
    }

    @GET
    @UnitOfWork
    fun getAccount(@QueryParam("id") id: Long): Response {
        val accountEntity = accountDAO.findById(id) ?: return Response.status(Response.Status.NOT_FOUND).build()
        return Response.ok()
                .entity(Account(accountEntity.id, accountEntity.balance)).build()
    }

    @GET
    @Path("/list")
    @UnitOfWork
    fun getAccounts(): Response {
        val accountEntities = accountDAO.findAll()
        val accounts = accountEntities.map { Account(it.id, it.balance) }
        return Response.ok()
                .entity(accounts).build()
    }

    @DELETE
    @UnitOfWork
    fun deleteAccount(@QueryParam("id") id: Long): Response {
        val accountEntity = accountDAO.findById(id)
        return when {
            accountEntity == null -> {
                Response.status(Response.Status.NOT_FOUND).build()
            }
            accountEntity.balance.compareTo(BigDecimal.ZERO) != 0 -> {
                Response.notModified().build()
            }
            else -> {
                accountDAO.delete(accountEntity)
                Response.ok().build()
            }
        }
    }
}