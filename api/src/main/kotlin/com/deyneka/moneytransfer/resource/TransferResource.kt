package com.deyneka.moneytransfer.resource

import com.deyneka.moneytransfer.api.TransferResult
import com.deyneka.moneytransfer.dao.AccountDAO
import com.deyneka.tools.ReentrantEntityLocker
import io.dropwizard.hibernate.UnitOfWork

import javax.ws.rs.POST
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Response
import java.math.BigDecimal
import java.util.concurrent.Callable
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/transfer")
@Produces(MediaType.APPLICATION_JSON)
class TransferResource(private val accountDAO: AccountDAO) {

    private val entityLocker = ReentrantEntityLocker<Long, TransferResult>()

    @POST
    @UnitOfWork
    fun transfer(@QueryParam("from") from: Long, @QueryParam("to") to: Long, @QueryParam("amount") amount: BigDecimal): Response {
        val transferResult = entityLocker.execute(
                listOf(from, to),
                naturalOrder(),
                Callable<TransferResult> {
                    return@Callable TransferResult(accountDAO.transfer(from, to, amount))
                }
        )
        return if (transferResult.success){
            Response.ok().entity(transferResult).build()
        } else {
            Response.notModified().entity(transferResult).build()
        }
    }
}
