package com.deyneka.moneytransfer.resource

import com.deyneka.moneytransfer.MoneyTransferApplication
import com.deyneka.moneytransfer.api.Account
import com.deyneka.moneytransfer.resource.helpers.AccountRequestHelper
import io.dropwizard.testing.ResourceHelpers
import io.dropwizard.testing.junit.DropwizardAppRule
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.ClassRule
import org.junit.Test
import java.math.BigDecimal
import javax.ws.rs.core.Response


class AccountResourceTest {

    private val accountResponseHelper = AccountRequestHelper(rule)

    companion object {
        @ClassRule
        @JvmField
        val rule = DropwizardAppRule(MoneyTransferApplication::class.java, ResourceHelpers.resourceFilePath("test-config.yml"))
    }

    @Test
    fun testCreateReadDelete(){
        val createdAccountResponse1 = accountResponseHelper.createAccount(Account(id=1, balance = BigDecimal("0.00")))
        Assert.assertEquals(createdAccountResponse1.readEntity(Account::class.java).balance, BigDecimal("0.00"))

        val createdAccountResponse2 = accountResponseHelper.createAccount(Account(id=2, balance = BigDecimal("200.00")))
        Assert.assertEquals(createdAccountResponse2.readEntity(Account::class.java).balance, BigDecimal("200.00"))

        val allAccountsResponse = accountResponseHelper.getAllAccounts()
        Assert.assertEquals(allAccountsResponse.readEntity(List::class.java).size, 2)

        val deletedResponse1 = accountResponseHelper.deleteAccount(1L)
        assertEquals(Response.Status.OK.statusCode, deletedResponse1.status)

        val deleteResponse2 = accountResponseHelper.deleteAccount(2L)
        assertEquals(Response.Status.NOT_MODIFIED.statusCode, deleteResponse2.status)

        val getByIdResponse1 = accountResponseHelper.getAccountById(1L)
        assertEquals(Response.Status.NOT_FOUND.statusCode, getByIdResponse1.status)

        val getByIdResponse2 = accountResponseHelper.getAccountById(2L)
        assertEquals(Response.Status.OK.statusCode, getByIdResponse2.status)
    }
}