package com.deyneka.moneytransfer.resource

import com.deyneka.moneytransfer.MoneyTransferApplication
import com.deyneka.moneytransfer.api.Account
import com.deyneka.moneytransfer.resource.helpers.AccountRequestHelper
import com.deyneka.moneytransfer.resource.helpers.TransferRequestHelper
import io.dropwizard.testing.ResourceHelpers.resourceFilePath
import io.dropwizard.testing.junit.DropwizardAppRule
import org.junit.Assert
import org.junit.ClassRule
import org.junit.Test
import java.math.BigDecimal
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.ws.rs.core.Response
import kotlin.random.Random

class TransferResourceConcurrentTest {

    private val accountRequestHelper = AccountRequestHelper(rule)
    private val transferRequestHelper = TransferRequestHelper(rule)

    companion object {
        @ClassRule
        @JvmField
        val rule = DropwizardAppRule(MoneyTransferApplication::class.java, resourceFilePath("test-config.yml"))
    }

    @Test
    fun testConcurrentTransfersBetweenTwoAccounts(){
        //create 2 accounts
        val createResponse1 = accountRequestHelper.createAccount(Account(id=1, balance = BigDecimal("1000.00")))
        val entity1 = createResponse1.readEntity(Account::class.java)
        Assert.assertEquals(BigDecimal("1000.00"), entity1.balance)

        val createResponse2 = accountRequestHelper.createAccount(Account(id=1, balance = BigDecimal("0.00")))
        val entity2 = createResponse2.readEntity(Account::class.java)
        Assert.assertEquals(BigDecimal("0.00"), entity2.balance)

        //perform concurrent money transfers
        val runnable = Runnable {
            for (i in 1..100) {
                val transferResult = transferRequestHelper.transfer(entity1.id, entity2.id, BigDecimal("1.00"))
                Assert.assertEquals(transferResult.status, Response.Status.OK.statusCode)
            }
        }
        val executorService = Executors.newCachedThreadPool()
        for (i in 1..10) {
            executorService.submit(runnable)
        }
        executorService.shutdown()
        try {
            executorService.awaitTermination(10000, TimeUnit.SECONDS)
        } catch (e: InterruptedException) {
            Assert.fail()
        }

        //check account balances
        val getResponse3 = accountRequestHelper.getAccountById(entity1.id)
        val entity3 = getResponse3.readEntity(Account::class.java)
        Assert.assertEquals(BigDecimal("0.00"), entity3.balance)

        val getResponse4 = accountRequestHelper.getAccountById(entity2.id)
        val entity4 = getResponse4.readEntity(Account::class.java)
        Assert.assertEquals(BigDecimal("1000.00"), entity4.balance)
    }

    @Test
    fun testConcurrentTransfersBetweenMultipleAccounts(){
        //create 3 accounts
        val createResponse1 = accountRequestHelper.createAccount(Account(id=1, balance = BigDecimal("1000.00")))
        val entity1 = createResponse1.readEntity(Account::class.java)
        Assert.assertEquals(BigDecimal("1000.00"), entity1.balance)

        val createResponse2 = accountRequestHelper.createAccount(Account(id=1, balance = BigDecimal("1000.00")))
        val entity2 = createResponse2.readEntity(Account::class.java)
        Assert.assertEquals(BigDecimal("1000.00"), entity2.balance)

        val createResponse3 = accountRequestHelper.createAccount(Account(id=1, balance = BigDecimal("1000.00")))
        val entity3 = createResponse3.readEntity(Account::class.java)
        Assert.assertEquals(BigDecimal("1000.00"), entity2.balance)

        val accountsBalanceSum = entity1.balance + entity2.balance + entity3.balance
        val entityIds = listOf(entity1.id, entity2.id, entity3.id)

        //perform concurrent money transfers
        val runnable = Runnable {
            for (i in 1..100) {
                transferRequestHelper.transfer(entityIds[Random.nextInt(0, 3)], entityIds[Random.nextInt(0, 3)], BigDecimal("100.00"))
            }
        }
        val executorService = Executors.newCachedThreadPool()
        for (i in 1..10) {
            executorService.submit(runnable)
        }
        executorService.shutdown()
        try {
            executorService.awaitTermination(10000, TimeUnit.SECONDS)
        } catch (e: InterruptedException) {
            Assert.fail()
        }
        //check balance sums
        val entity4 = accountRequestHelper.getAccountById(entity1.id).readEntity(Account::class.java)
        val entity5 = accountRequestHelper.getAccountById(entity2.id).readEntity(Account::class.java)
        val entity6 = accountRequestHelper.getAccountById(entity3.id).readEntity(Account::class.java)
        Assert.assertEquals(accountsBalanceSum, entity4.balance + entity5.balance + entity6.balance)
    }
}