package com.deyneka.moneytransfer.resource

import com.deyneka.moneytransfer.MoneyTransferApplication
import com.deyneka.moneytransfer.api.Account
import com.deyneka.moneytransfer.api.TransferResult
import com.deyneka.moneytransfer.resource.helpers.AccountRequestHelper
import com.deyneka.moneytransfer.resource.helpers.TransferRequestHelper
import io.dropwizard.testing.ResourceHelpers.resourceFilePath
import io.dropwizard.testing.junit.DropwizardAppRule
import org.junit.Assert.assertEquals
import org.junit.ClassRule
import org.junit.Test
import java.math.BigDecimal
import javax.ws.rs.core.Response


class TransferResourceTest {
    private val accountRequestHelper = AccountRequestHelper(rule)
    private val transferRequestHelper = TransferRequestHelper(rule)

    companion object {
        @ClassRule
        @JvmField
        val rule = DropwizardAppRule(MoneyTransferApplication::class.java, resourceFilePath("test-config.yml"))
    }

    @Test
    fun testSimpleValidTransfer(){
        val account1 = createAccount(BigDecimal("100.00"))
        val account2 = createAccount(BigDecimal("200.00"))

        val transferResponse = transferRequestHelper.transfer(account1.id, account2.id, BigDecimal("50.00"))
        val transferResult = transferResponse.readEntity(TransferResult::class.java)
        assertEquals(true, transferResult.success)

        val changedAccount1 = accountRequestHelper.getAccountById(account1.id).readEntity(Account::class.java)
        assertEquals(BigDecimal("50.00"), changedAccount1.balance)

        val changedAccount2 = accountRequestHelper.getAccountById(account2.id).readEntity(Account::class.java)
        assertEquals(BigDecimal("250.00"), changedAccount2.balance)
    }

    @Test
    fun testSimpleInvalidTransfer(){
        val account1 = createAccount(BigDecimal("100.00"))
        val account2 = createAccount(BigDecimal("200.00"))

        val invalidTransferResponse1 = transferRequestHelper.transfer(account1.id, account2.id, BigDecimal("1000.00"))
        assertEquals(Response.Status.NOT_MODIFIED.statusCode, invalidTransferResponse1.status)
    }

    @Test
    fun testTransferBetweenOneAccount(){
        val account1 = createAccount(BigDecimal("100.00"))

        val transferResponse1 = transferRequestHelper.transfer(account1.id, account1.id, BigDecimal("50.00"))
        assertEquals(transferResponse1.status, Response.Status.NOT_MODIFIED.statusCode)
    }

    @Test
    fun testTransferDecimalValues(){
        val account1 = createAccount(BigDecimal("100.12"))
        val account2 = createAccount(BigDecimal("200.29"))

        val transferResponse1 = transferRequestHelper.transfer(account1.id, account2.id, BigDecimal("50.66"))
        val transferResult1 = transferResponse1.readEntity(TransferResult::class.java)
        assertEquals(true, transferResult1.success)

        val changedAccount1 = accountRequestHelper.getAccountById(account1.id).readEntity(Account::class.java)
        assertEquals(BigDecimal("49.46"), changedAccount1.balance)

        val changedAccount2 = accountRequestHelper.getAccountById(account2.id).readEntity(Account::class.java)
        assertEquals(BigDecimal("250.95"), changedAccount2.balance)
    }

    @Test
    fun testTransferNegativeAmount(){
        val account1 = createAccount(BigDecimal("100.00"))
        val account2 = createAccount(BigDecimal("200.00"))

        val invalidTransferResponse1 = transferRequestHelper.transfer(account1.id, account2.id, BigDecimal("-50.00"))
        assertEquals(Response.Status.NOT_MODIFIED.statusCode, invalidTransferResponse1.status)

        val changedAccount1 = accountRequestHelper.getAccountById(account1.id).readEntity(Account::class.java)
        assertEquals(BigDecimal("100.00"), changedAccount1.balance)

        val changedAccount2 = accountRequestHelper.getAccountById(account2.id).readEntity(Account::class.java)
        assertEquals(BigDecimal("200.00"), changedAccount2.balance)
    }

    @Test
    fun testTransferNullAmount(){
        val account1 = createAccount(BigDecimal("100.00"))
        val account2 = createAccount(BigDecimal("200.00"))

        val invalidTransferResponse1 = transferRequestHelper.transfer(account1.id, account2.id, null)
        assertEquals(Response.Status.INTERNAL_SERVER_ERROR.statusCode, invalidTransferResponse1.status)

        val changedAccount1 = accountRequestHelper.getAccountById(account1.id).readEntity(Account::class.java)
        assertEquals(BigDecimal("100.00"), changedAccount1.balance)

        val changedAccount2 = accountRequestHelper.getAccountById(account2.id).readEntity(Account::class.java)
        assertEquals(BigDecimal("200.00"), changedAccount2.balance)
    }

    @Test
    fun testTransferFromAndToNotExistingAccount(){
        val account1 = createAccount(BigDecimal("100.00"))

        val invalidTransferResponse1 = transferRequestHelper.transfer(account1.id, 555, BigDecimal("50.00"))
        assertEquals(Response.Status.NOT_MODIFIED.statusCode, invalidTransferResponse1.status)
    }

    private fun createAccount(balance: BigDecimal): Account {
        val createResponse1 = accountRequestHelper.createAccount(Account( balance = balance ))
        val account = createResponse1.readEntity(Account::class.java)
        assertEquals(balance, account.balance)
        return account
    }
}