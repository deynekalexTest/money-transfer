package com.deyneka.moneytransfer.resource.helpers

import com.deyneka.moneytransfer.MoneyTransferConfiguration
import com.deyneka.moneytransfer.api.Account
import io.dropwizard.testing.junit.DropwizardAppRule
import org.glassfish.jersey.client.ClientConfig
import org.glassfish.jersey.client.ClientProperties
import org.glassfish.jersey.client.JerseyClientBuilder
import org.junit.Assert
import javax.ws.rs.client.Client
import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

class AccountRequestHelper(private val rule: DropwizardAppRule<MoneyTransferConfiguration>) {
    private val host = "http://127.0.0.1"
    private val apiEndpoint = "moneytransfer"

    fun getAccountById(id: Long): Response {
        return client().target("$host:${rule.localPort}/$apiEndpoint/account")
                .queryParam("id", id)
                .request().get()
    }

    fun deleteAccount(id: Long): Response {
        return client().target("$host:${rule.localPort}/$apiEndpoint/account")
                .queryParam("id", id)
                .request().delete()
    }

    fun createAccount(account: Account): Response {
        val createResponse = client().target("$host:${rule.localPort}/$apiEndpoint/account")
                .request().post(Entity.entity(account, MediaType.APPLICATION_JSON), Response::class.java)
        Assert.assertEquals(Response.Status.OK.statusCode, createResponse.status)
        return createResponse
    }

    fun getAllAccounts(): Response {
        val getAllResponse = client().target("$host:${rule.localPort}/$apiEndpoint/account/list")
                .request().get()
        Assert.assertEquals(Response.Status.OK.statusCode, getAllResponse.status)
        return getAllResponse
    }

    private fun client(): Client {
        val config = ClientConfig()
        config.property(ClientProperties.CONNECT_TIMEOUT, 5000)
        config.property(ClientProperties.READ_TIMEOUT, 15000)
        return JerseyClientBuilder.createClient(config)
    }
}