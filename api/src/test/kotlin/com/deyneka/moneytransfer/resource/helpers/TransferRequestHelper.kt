package com.deyneka.moneytransfer.resource.helpers

import com.deyneka.moneytransfer.MoneyTransferConfiguration
import io.dropwizard.testing.junit.DropwizardAppRule
import org.glassfish.jersey.client.ClientConfig
import org.glassfish.jersey.client.ClientProperties
import org.glassfish.jersey.client.JerseyClientBuilder
import java.math.BigDecimal
import javax.ws.rs.client.Client
import javax.ws.rs.client.Entity
import javax.ws.rs.core.Response

class TransferRequestHelper(private val rule: DropwizardAppRule<MoneyTransferConfiguration>) {
    private val host = "http://127.0.0.1"
    private val apiEndpoint = "moneytransfer"

    fun transfer(from: Long, to: Long, amount: BigDecimal?): Response {
        return client().target("$host:${rule.localPort}/$apiEndpoint/transfer")
                .queryParam("from", from)
                .queryParam("to", to)
                .queryParam("amount", amount)
                .request().post(Entity.json(null), Response::class.java)
    }

    private fun client(): Client {
        val config = ClientConfig()
        config.property(ClientProperties.CONNECT_TIMEOUT, 5000)
        config.property(ClientProperties.READ_TIMEOUT, 15000)
        return JerseyClientBuilder.createClient(config)
    }
}