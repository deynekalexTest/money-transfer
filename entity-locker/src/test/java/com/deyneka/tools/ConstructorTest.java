package com.deyneka.tools;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ConstructorTest {
    @Test
    public void checkNullArguments(){
        Assert.assertThrows(IllegalArgumentException.class, () -> new ReentrantEntityLocker<>(0));
        Assert.assertThrows(IllegalArgumentException.class, () -> new ReentrantEntityLocker<>(-1));
        new ReentrantEntityLocker<>(1);
    }
}
